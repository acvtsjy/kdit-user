using System.Net;
using System.Net.Mail;
using Azure.Data.Tables;
using Microsoft.Extensions.Configuration;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;
using Azure.Identity;
using System.Text.Json;
using System.IO;


namespace Cloutomate
{
  public class Trigger
  {
    private readonly IConfiguration _config;
    private readonly ILogger _logger;
    private const string tableNameUser = "User";
    private readonly TableClient _clientUser;

    private const string tableNameCategory = "Category";
    private readonly TableClient _clientCategory;


    public Trigger(ILoggerFactory loggerFactory, IConfiguration config)
    {
      _config = config;
      _logger = loggerFactory.CreateLogger<Trigger>();
      var storage = config.GetValue<string>("StorageAccount") ?? "";
      if (string.IsNullOrEmpty(storage))
      {
        // use debug mode (Azurite)
        _clientUser = new TableClient("DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;TableEndpoint=http://127.0.0.1:10002/devstoreaccount1;", tableNameUser);
        _clientCategory = new TableClient("DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;TableEndpoint=http://127.0.0.1:10002/devstoreaccount1;", tableNameCategory);

      }
      else
      {
        _clientUser = new TableClient(new Uri(storage), tableNameUser, new DefaultAzureCredential());
        _clientCategory = new TableClient(new Uri(storage), tableNameCategory, new DefaultAzureCredential());
      }
      // ensure table exists
      // _clientT.CreateIfNotExists();
      _clientUser.CreateIfNotExists();
      _clientCategory.CreateIfNotExists();

    }



    [Function("GetUsers")]
    public HttpResponseData GetUsers(
      [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "User")] HttpRequestData req
    )
    {
      var res = _clientUser.Query<User>().ToList();
      var response = req.CreateResponse(HttpStatusCode.OK);
      response.WriteAsJsonAsync(res);
      return response;
    }

    [Function("CreateUser")]
    public HttpResponseData CreateUser(
          [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "User")] HttpRequestData req,
          [FromBody] User User
        )
    {
      // set custom id
      User.RowKey = Guid.NewGuid().ToString();
      _clientUser.AddEntity(User);

      var response = req.CreateResponse(HttpStatusCode.OK);
      response.WriteAsJsonAsync(User);

      return response;
    }

    [Function("UpdateUser")]
    public HttpResponseData UpdateUser(
          [HttpTrigger(AuthorizationLevel.Anonymous, "patch", Route = "User/{id}")] HttpRequestData req,
          string id,
          [FromBody] User User
        )
    {
      var res = _clientUser.Query<User>().Where(User => User.RowKey == id).FirstOrDefault(); //ToList()
      if (res == null)
      {
        var response1 = req.CreateResponse(HttpStatusCode.NotFound);
        response1.WriteAsJsonAsync(res);
        return response1;
      }

      _clientUser.UpdateEntity<User>(User, Azure.ETag.All, TableUpdateMode.Replace);

      var response = req.CreateResponse(HttpStatusCode.OK);
      response.WriteAsJsonAsync(res);
      return response;
    }

    [Function("DeleteUser")]
    public HttpResponseData DeleteUser(
      [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "User/{id}")] HttpRequestData req,
      string id)
    {
      
      _clientUser.DeleteEntity("static", id);
      return req.CreateResponse(HttpStatusCode.NoContent);
    }



    [Function("GetAllCategory")]
    public HttpResponseData GetAllCategory(
      [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "Category")] HttpRequestData req
    )
    {
      var res = _clientCategory.Query<Category>().ToList();
      var response = req.CreateResponse(HttpStatusCode.OK);
      response.WriteAsJsonAsync(res);
      return response;
    }


    [Function("GetCategoryById")]
    public HttpResponseData GetCategoryById(
      [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "Category({idCat})")] HttpRequestData req,
      string idCat     
    )
    {
      var res = _clientCategory.Query<Category>().Where (cat => cat.RowKey == idCat).FirstOrDefault();
      var response = req.CreateResponse(HttpStatusCode.OK);
      response.WriteAsJsonAsync(res);
      return response;
    }

    [Function("CreateCategory")]
    public HttpResponseData CreateCategory(
          [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "Category")] HttpRequestData req,
          [FromBody] Category Category
        )
    {
      // set custom id
      Category.RowKey = Guid.NewGuid().ToString();
      _clientCategory.AddEntity(Category);

      var response = req.CreateResponse(HttpStatusCode.OK);
      response.WriteAsJsonAsync(Category);

      return response;
    }

    [Function("DeleteCategory")]
    public HttpResponseData DeleteCategory(
      [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "Category/{idCat}")] HttpRequestData req,
      string idCat)
    {
      _clientCategory.DeleteEntity("static", idCat);
      return req.CreateResponse(HttpStatusCode.NoContent);
    }

  }
}