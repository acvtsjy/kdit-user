/// <reference types="vite/client" />

declare const __API_MOCK__ : boolean;
declare const __API_SERVER__ : string;
declare const __CLIENT_ID__ : string;
declare const __CLIENT_TENANT__ : string;
