// router.ts

import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '@/views/HomePage.vue'
import UserList from '@/components/UserList.vue';
import UserCreate from '@/components/UserCreate.vue';

// Importa i componenti delle tue pagine
import AboutPage from '@/views/About.vue'
// Altri componenti delle pagine...
const routes = [
  {
    path: '/', // Percorso per la home
    component: HomePage, // Componente per la home
    name: 'HomePage' // Nome della rotta per la home
  },
  {
    path: '/userlist',
    component: UserList,
    name: 'UserList'
  },
  {
    path: '/usercreate',
    component: UserCreate,
    name: 'UserCreate'
  },
  {
    path: '/about',
    component: AboutPage,
    name: 'AboutPage'
  },
  // Altre rotte...
]


const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
