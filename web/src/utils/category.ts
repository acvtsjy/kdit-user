import type { Category } from "../api";
import { ref, type Ref } from "vue";
import { computed } from "vue";
import { getAllCategory, createCategory } from "../api";

export const categories = <Ref<Category[]>>ref([]);

export const reloadCategories = () => getAllCategory().then((c) => {
  (categories.value = c)
});

// export function loadCategories() {
//   reloadCategories();
// }
// getAllCategory().then((c) => {
//   categories.value = c
// });

// export const oneEmployee = <Ref<Employees>>ref();


