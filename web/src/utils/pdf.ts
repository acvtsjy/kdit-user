import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from "pdfmake/build/vfs_fonts";
import type { TDocumentDefinitions } from "pdfmake/interfaces";
// Converte pdfMake per consentire l'aggiornamento del vfs in sola lettura con i font importati
//(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;
(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;

export interface PdfDocument {
  content: any[]; // Struttura specifica del contenuto del PDF
  styles?: any; // Opzionale, stili personalizzati
  defaultStyle?: any; // Opzionale, stile predefinito
}

export function downloadPDF(dd: PdfDocument, filename: string) {
  // Crea e scarica il PDF utilizzando pdfMake
  pdfMake.createPdf(dd).download(filename);
}


