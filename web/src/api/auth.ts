import type { TokenCredential, AccessToken } from "@azure/core-auth";
import * as msalBrowser from "@azure/msal-browser";

/**
 * Credential Helper Class  
 */
class BrowserCredential implements TokenCredential {
  private publicApp: msalBrowser.PublicClientApplication;
  private hasAuthenticated: boolean = false;

  constructor(clientId : string, redirectUri : string) {
    this.publicApp = new msalBrowser.PublicClientApplication({
      auth: {
        authority: `https://login.microsoftonline.com/${__CLIENT_TENANT__}`,
        clientId,
        redirectUri,
      }
    });
  }

  // Either confirm the account already exists in memory, or tries to parse the redirect URI values.
  async prepare(): Promise<void> {
    try {
      await this.publicApp.initialize();
      const account = this.publicApp.getActiveAccount();
      if (account !== null) {
        this.hasAuthenticated = true;
        return;
      }
      await this.publicApp.handleRedirectPromise().then(result => {
        this.hasAuthenticated = true;
        if(!!result) {
          this.publicApp.setActiveAccount(result.account);
          return;
        }
        const accounts = this.publicApp.getAllAccounts();
        if(accounts && accounts.length){
          this.publicApp.setActiveAccount(accounts[0]);
          return;
        }
        this.hasAuthenticated = false;
      });
    } catch (e) {
      console.error("BrowserCredential prepare() failed", e);
    }
  }

  // Should be true if prepare() was successful.
  isAuthenticated(): boolean {
    return this.hasAuthenticated;
  }

  // If called, triggers authentication via redirection.
  async loginRedirect(scopes: string | string[]): Promise<void> {
    const loginRequest = {
      scopes: Array.isArray(scopes) ? scopes : [scopes]
    };
    await this.publicApp.loginRedirect(loginRequest);
  }

  // Tries to retrieve the token without triggering a redirection.
  async getToken(scopes: string | string[]): Promise<AccessToken> {
    if (!this.hasAuthenticated) {
      throw new Error("Authentication required");
    }

    const account  = this.publicApp.getActiveAccount();
    const parameters: msalBrowser.SilentRequest = {
      account: account === null ? undefined : account,
      scopes: "string" === typeof scopes ? [scopes] : scopes,
    };

    const result = await this.publicApp.acquireTokenSilent(parameters);
    return {
      token: result.accessToken,
      expiresOnTimestamp: (result.expiresOn ?? new Date()).getTime()
    };
  }
}

// is authentication enabled
export const authEnabled = "string" === typeof __CLIENT_ID__ && __CLIENT_ID__.length;
// AAD default scope
const scope = `${__CLIENT_ID__}/.default`;
let browserCredential : BrowserCredential;

if(authEnabled){
  // create new default instance of helper class
  browserCredential = new BrowserCredential(__CLIENT_ID__, location.origin);
}

let initializePromise : Promise<void> | null = null;

/**
 * retrieve OAuth Token from AAD
 * @returns OAuth Token or empty string if disabled
 */
export async function getToken(){
  if(!authEnabled) {
    return "";
  }
  if(!initializePromise) {
    initializePromise = browserCredential.prepare();
  }
  await initializePromise;
  if (!browserCredential.isAuthenticated()) {
    // not logged in
    await browserCredential.loginRedirect(scope);
  }
  // retrieve token
  const token = await browserCredential.getToken(scope);
  return token.token;
}
