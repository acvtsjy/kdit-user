import type { User } from "./openapi/models/User";
import type { Category } from "./openapi/models/Category";

let count = 0;
const dbUser : User[] = [];

[
  {
    dealer: "TUTTOUFFICIO",
    sapCode: "0710003456",
    username: "info@tuttoufficio.com",
    dealerName: "Pippo",
    password: "Password",
    salesman: "raffaele.vigano@dit.kyocera.com",
    idCat: "idC_1",
    enabled: true,
  },
  {
    dealer: "INFOSISTEMI",
    sapCode: "0710002789",
    username: "info@infosistemi.com",
    dealerName: "Pippo",
    password: "password",
    salesman: "raffaele.vigano@dit.kyocera.com",
    idCat: "idC_0",
    enabled: true,
  },
].forEach(c => createUser(c));

let countC = 0;
const dbCat : Category[] = [];

[ "KEP",
  "GOLD PARTNER",  
  "SILVER PARTNER",
  "PLATINUM PARTNER",
  "SIER INDIRECT",
  "DISTR HW",
  "DISTR SUPPLY",
  "RESELLER"]
  .forEach(createCategory);

/**
 * creates a User item
 *
 * @returns created User item
 */


export async function createCategory(categoryName: string): Promise<Category> {
  const cat = {idCat : `idC_${countC++}`, categoryName};
  dbCat.push(cat);
  return cat;
}

export async function getCategoryById(id: string): Promise<string | undefined> {
  const oneCategory = dbCat.find((cat) => cat.idCat === id);
  return oneCategory ? oneCategory.categoryName : undefined;
}
 

export async function getAllCategory(): Promise<Category[]> {
  const dbCopyCat: Category[] = JSON.parse(JSON.stringify(dbCat));
  return dbCopyCat;
}

export async function createUser(user: User): Promise<User> {

  console.log(`Mock create user ${user}`);

  if (!user || !user.password) {
    throw new Error('Invalid input: User object or password is missing.');
  }
  console.log(`Password controllata create user ${user.dealer}`);

  // Hash della password

  // Creazione del nuovo utente con la password hash
  const newUser: User = {
    ...user,
    id: `id_${count++}`,

  };

  newUser.dealer = newUser.dealer?.toUpperCase();
  newUser.enabled = true;

  console.log(`newUser  ${newUser}`);
  dbUser.push(newUser);

  return newUser;
}

export async function getUsers(): Promise<User[]> {
  const dbCopy: User[] = JSON.parse(JSON.stringify(dbUser));
  return dbCopy;
}

export async function updateUser(uUser: User): Promise<User> {
  // Trova l'elemento con l'id specificato
  const userUpdate = dbUser.find((User) => User.id === uUser.id);
  if (!userUpdate) {
    // no such id in database
    throw new Error(`no element with id ${uUser.id} found`);
  }

  // Aggiorna la descrizione dell'elemento
  userUpdate.sapCode = uUser.sapCode
  userUpdate.username = uUser.username
  userUpdate.dealerName = uUser.dealerName
  userUpdate.password = uUser.password;
  // Hash della nuova password se presente
 
  userUpdate.password = uUser.password;
  userUpdate.salesman = uUser.salesman;
  userUpdate.idCat = uUser.idCat;


  return userUpdate;
}

export async function deleteUser(id: string): Promise<void> {
  // check if element was existing
  const index = dbUser.findIndex((User) => User.id === id);
  if (index < 0) {
    // no such id in database
    throw new Error(`no element with id ${id} found`);
  }
  // remove item
  dbUser.splice(index, 1);
}

export async function deleteCategory(idCat: string): Promise<void> {
  // check if element was existing
  const index = dbCat.findIndex((Category) => Category.idCat === idCat);
  if (index < 0) {
    // no such id in database
    throw new Error(`no element with id ${idCat} found`);
  }
  // remove item
  dbCat.splice(index, 1);
}

export async function getCategoryByIdSync(id: string): Promise<string | undefined> {
  const category = dbCat.find((cat) => cat.idCat === id);
  return category ? category.categoryName : 'Categoria non specificata';
}

export async function searchDealer(searchString: string): Promise<User[]> {
  if (searchString == undefined) {
     throw new Error("undefined");
     
  }
  console.log(dbUser);
  console.log(searchString.toUpperCase());
  const filteredUsers = dbUser.filter(user => user.dealer?.startsWith(searchString.toUpperCase()));
  console.log(filteredUsers);

  if (!filteredUsers) {
    throw new Error (`no element with id ${searchString} found`);
  } 
  
  return filteredUsers;

  //   const dbCopy : Cig[] = JSON.parse(JSON.stringify(db));
  // return dbCopy;
}