import {
  Configuration,
  DefaultApi,
  
  type CreateUserRequest,  
  type UpdateUserRequest,
  type User, 
  type Category,
  type GetCategorybyIdRequest,
  type CreateCategoryRequest

} from "./openapi";
import { getToken } from "./auth";
//from openapi
let token: string = "";
let preparePromise: Promise<void> | null = null;
let api: DefaultApi;

// prepare ensures a valid token is present
async function prepare() {
  if (preparePromise == null) {
    preparePromise = getToken().then((t) => {
      api = new DefaultApi(
        new Configuration({
          basePath: __API_SERVER__,
          headers: {
            Authorization: `Bearer ${t}`,
          },
        })
      );
    });
  }
  return preparePromise;
}

export async function createCategory(categoryName: string): Promise<Category> {
  //createContract({codContratto: codContratto.value})
  await prepare();
    
  const requestParameters: CreateCategoryRequest = {
    categoryName: categoryName,
  };
  console.log('requestP', requestParameters.categoryName);
  const response = await api.createCategoryRaw(requestParameters);
  return await response.value();
}


export async function deleteCategory(idCat: string): Promise<void> {
  await prepare();
  return api.categoryDelete({ idCat });
}



export async function getAllCategory(): Promise<Category[]> {
  await prepare();
  return api.getAllCategory();
}
export async function getCategoryById(id: string): Promise<Category> {
  await prepare();
  const requestParameters: GetCategorybyIdRequest = {
    idCat: id,
  }
  return api.getCategorybyId(requestParameters);
}

export async function getUsers(): Promise<User[]> {
  await prepare();
  return api.getUsers();
}

export async function createUser(nUser: User): Promise<User> {
  //createContract({codContratto: codContratto.value})
  await prepare();
    
  nUser.dealer = nUser.dealer?.toUpperCase();  
  nUser.enabled = true;
  console.log(nUser);

  const requestParameters: CreateUserRequest = {
    user: nUser,
  };

  return api.createUser( requestParameters );
}


export async function updateUser(uUser: User): Promise<User> {
  await prepare(); // Assicurati che questa funzione faccia quello che è necessario prima della chiamata API

  console.log('uUser', uUser);

  if (uUser.id === undefined) {
    throw new Error(`'id' not defined`);
  }

  // Log aggiuntivo per debug
  console.log('user before requestParameters creation', uUser);

  // Crea l'oggetto di richiesta con id e l'oggetto User aggiornato
  const requestParameters: UpdateUserRequest = {
    id: uUser.id,
    user: uUser
  };

  // Log aggiuntivo per debug
  console.log('requestParameters', requestParameters);

  // Effettua la chiamata API
  return api.updateUser(requestParameters);
}



/***
 * delete User item
 * @param id id of element to remove
 */
export async function deleteUser(id: string): Promise<void> {
  await prepare();
  return api.userDelete({ id });
}


export async function searchDealer(nameDealer: string): Promise<User[]> {
  await prepare();
  return api.searchDealer( { dealer: nameDealer})
  
}