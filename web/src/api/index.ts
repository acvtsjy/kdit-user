// export type definition
export type { User } from "./openapi/models/User";
export type { Category } from "./openapi/models/Category";

export * from "./auth";


import * as APIMock from "./mock";
import * as APIServer from "./server";

// export mock or actual API  
export const createUser = __API_MOCK__ ? APIMock.createUser : APIServer.createUser;
export const deleteUser = __API_MOCK__ ? APIMock.deleteUser : APIServer.deleteUser;
export const getUsers = __API_MOCK__ ? APIMock.getUsers : APIServer.getUsers;
export const updateUser = __API_MOCK__ ? APIMock.updateUser : APIServer.updateUser;
export const searchDealer = __API_MOCK__ ? APIMock.searchDealer : APIMock.searchDealer;

export const createCategory = __API_MOCK__ ? APIMock.createCategory : APIServer.createCategory;
export const getAllCategory = __API_MOCK__ ? APIMock.getAllCategory : APIServer.getAllCategory;
export const getCategoryById = __API_MOCK__ ? APIMock.getCategoryById : APIServer.getCategoryById;
export const deleteCategory = __API_MOCK__ ? APIMock.deleteCategory : APIServer.deleteCategory;

if (__API_MOCK__) {
  console.warn("API Mock is enabled!");
}
