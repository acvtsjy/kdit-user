import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// remote API endpoint
const apiRemote = "https://myusermanagement.azurewebsites.net/api" //change the name
// local API endpoint (when developing)
const apiLocal ="http://localhost:7071/api";  //apiRemote; //
// should we mock the API when in debug mode?
const mockApi = true;
// AAD Client ID (set to emptry string to disable)
const AAD_CLIENT = "";
// AAD Tenant ID (set to empty string to disable)
const AAD_TENANT = "";

// https://vitejs.dev/config/
export default defineConfig(({ command }) => {
  const isBuild = command === "build";
  return {
    plugins: [vue()],
    resolve: {
      alias: {
        "@": fileURLToPath(new URL("./src", import.meta.url)),
      },
    },
    define: {
      __API_MOCK__: JSON.stringify(isBuild ? false : mockApi),
      __API_SERVER__: JSON.stringify(isBuild ? apiRemote : apiLocal),
      __CLIENT_ID__: JSON.stringify(AAD_CLIENT),
      __CLIENT_TENANT__: JSON.stringify(AAD_TENANT),
    },
  };
});
